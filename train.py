import joblib
from sklearn import tree,metrics
import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.metrics import f1_score
from sklearn.metrics import classification_report
from sklearn.preprocessing import StandardScaler
from sklearn.metrics import accuracy_score
from sklearn.tree import DecisionTreeClassifier

if __name__ == '__main__':
	# Train multi class classification model and save it to the working directory
	#DATA_DIR = 'c:\\Users\\Moisa\\skillfactory\\Нир\\нир 2\\'
	df = pd.read_excel('РСХБ_new1.xlsx',sheet_name ='IQ',index_col="НОМЕР")
	df.rename(columns={'С1: "ЧИСЛОВОЙ IQ" - СТЭН': 'ЧИСЛОВОЙ IQ','С1: "ЧИСЛОВОЙ IQ" - СТЭН': 'ЧИСЛОВОЙ IQ', 'С3: "ВНИМАНИЕ" - СТЭН': 'ВНИМАНИЕ', 'С5: "ПОНИМАНИЕ ТЕКСТОВ" - СТЭН': 'ПОНИМАНИЕ ТЕКСТОВ', 'С7: "ВЕРБАЛЬНАЯ ЛОГИКА" - СТЭН': 'ВЕРБАЛЬНАЯ ЛОГИКА', 'С9: "ОБЩИЙ БАЛЛ" - СТЭН': 'ОБЩИЙ БАЛЛ'}, inplace=True)
	df = df.fillna(0)
	#Замена целевой переменной
	df = df.drop(['ФАМИЛИЯ '], axis=1)
	df.Статус = df.Статус.replace('неуспешные',0)
	df.Статус = df.Статус.replace('успешные',1)
	df.ПОЛ = df.ПОЛ.replace('женский',0)
	df.ПОЛ = df.ПОЛ.replace('мужской',1)
	df.Офис = df.Офис.replace('фронт',1)
	df.Офис = df.Офис.replace('бэк',0)
	#df.head(10)
	X = df.drop(['Статус',], axis=1)
	y = df.Статус
	#X = pd.DataFrame(scaler.fit_transform(X), columns=X.columns)
	#X.head()
	X_train, X_test, y_train, y_test = train_test_split(X,y,test_size = 0.2,random_state=42)
	#Деревья решений 
	
	clf = GradientBoostingClassifier(learning_rate=0.1, n_estimators=100,
                                 max_depth=3, min_samples_split=2, min_samples_leaf=1,
                                 subsample=1, max_features=None)
	clf.fit(X_train, y_train)
	joblib.dump(clf, "./model.joblib")
