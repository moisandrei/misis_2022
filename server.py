import joblib
import uvicorn
from fastapi import FastAPI
from pydantic import BaseModel

# App creation and model loading
app = FastAPI()
model = joblib.load("./model.joblib")


class IQSpecies(BaseModel):
	"""
	Input features validation for the ML model
	"""
	RESULT: float
	Office: float
	AGE: float
	GENDER: float

	probation_year: float
	NUMERICAL_IQ: float
	ATTENTION: float
	UNDERSTANDING_OF_TEXTS: float
	VERBAL_LOGIC: float
	TOTAL_CORE: float


@app.post('/predict')
def predict(iq: IQSpecies):
	"""
	:param IQ: input data from the post request
	:return: predicted IQ type
	"""
	features = [[
		iq.RESULT,
        iq.Office,
        iq.AGE,
		iq.GENDER,
        iq.probation_year,
		iq.NUMERICAL_IQ,
		iq.ATTENTION,
		iq.UNDERSTANDING_OF_TEXTS,
		iq.VERBAL_LOGIC,
		iq.TOTAL_CORE
	]]
	prediction = model.predict(features).tolist()[0]
	return {
		"prediction": prediction
	}


if __name__ == '__main__':
	# Run server using given host and port
	uvicorn.run(app, host='51.250.102.223', port=80)
